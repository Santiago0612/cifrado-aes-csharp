﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

class Program
{
    private static readonly byte[] Key = Encoding.UTF8.GetBytes("1234567890abcdef"); 
    private static readonly byte[] IV = Encoding.UTF8.GetBytes("abcdef1234567890");

    static void Main(string[] args)
    {
        string original = "https://76ad-181-62-56-8.ngrok-free.app/MC/api/";

        // Cifra la URL
        string encrypted = EncryptString(original);

        Console.WriteLine($"Texto cifrado: {encrypted}");

        // Guarda el texto cifrado en un archivo .txt
        File.WriteAllText(@"C:\Users\santi\Desktop\url.txt", encrypted);
        Console.WriteLine("Archivo guardado con éxito.");
    }

    static string EncryptString(string plainText)
    {
        using (Aes aesAlg = Aes.Create())
        {
            aesAlg.Key = Key;
            aesAlg.IV = IV;

            ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using StreamWriter swEncrypt = new StreamWriter(csEncrypt);
                    swEncrypt.Write(plainText);
                }

                return Convert.ToBase64String(msEncrypt.ToArray());
            }
        }
    }
}